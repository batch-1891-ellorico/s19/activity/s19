console.log("Hello World")

// LOG-IN FUNCTION

function logIn() {
	
	let username = prompt("Please enter your username:")
		if (username <= 0) {
			alert ('Input should not be empty.')
		} else {
			console.log(username)
		}

	let password = prompt("Please enter your password:")
		if (password <= 0) {
			alert ('Input should not be empty.')
		} else {
			console.log(password)
		}

	let role = prompt("Please enter role:")
		if (role === 'admin') {
			alert ('Welcome back to the class portal, admin!')
		} else if (role === 'teacher') {
			alert ('Thank you for logging in, teacher!')
		} else if (role === 'student') {
			alert ('Welcome to the class portal, student!')
		} else {
			console.log("Role out of range.")
		}

}
logIn ()


// AVERAGE CALCULATOR

function averageCalculator(num1, num2, num3, num4){
	let average = 0
	average = ((num1 + num2 + num3 + num4)/4)
	console.log('checkAverage (' + num1 + "," + num2 + "," + num3 + "," + num4 + ")")
	roundedAverage = Math.round(average)
	if (roundedAverage <= 74){
		console.log(`Hello student, your average is ${roundedAverage}. The letter equivalent is F`)
	} else if (roundedAverage >=75 && roundedAverage <= 79) {
		console.log(`Hello student, your average is ${roundedAverage}. The letter equivalent is D`)
	} else if (roundedAverage >= 80 && roundedAverage <= 84) {
		console.log(`Hello student, your average is ${roundedAverage}. The letter equivalent is C`)
	} else if (roundedAverage >= 85 && roundedAverage <= 89) {
		console.log(`Hello student, your average is ${roundedAverage}. The letter equivalent is B`)
	} else if (roundedAverage >=90 && roundedAverage <= 95) {
		console.log(`Hello student, your average is ${roundedAverage}. The letter equivalent is A`)
	} else if (roundedAverage >= 95) {
		console.log(`Hello student, your average is ${roundedAverage}. The letter equivalent is A+`)
	}
}
averageCalculator(91, 99, 98, 99)